# find-sec-bugs Scala sbt analyzer changelog

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Add `Scanner` property and deprecate `Tool`

## v1.1.0
- Show command error output

## v1.0.0
- initial release
