FROM  openjdk:8-jdk-alpine

ARG SCALA_VERSION
ARG SBT_VERSION
ARG FSB_VERSION

ENV SCALA_VERSION ${SCALA_VERSION:-2.12.6}
ENV SBT_VERSION ${SBT_VERSION:-1.1.4}
ENV FSB_VERSION ${FSB_VERSION:-1.7.1}

RUN apk add --no-cache bash curl git openssh-client

# Install Scala
RUN curl -fsL https://downloads.typesafe.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz | tar xfz - -C /usr/local
RUN ln -s /usr/local/scala-$SCALA_VERSION/bin/* /usr/local/bin/

# Install Sbt
RUN curl -fsL https://github.com/sbt/sbt/releases/download/v$SBT_VERSION/sbt-$SBT_VERSION.tgz | tar xfz - -C /usr/local
RUN ln -s /usr/local/sbt/bin/* /usr/local/bin/

# Install Find Security Bugs
ADD https://github.com/find-sec-bugs/find-sec-bugs/releases/download/version-${FSB_VERSION}/findsecbugs-cli-${FSB_VERSION}.zip /tmp/findsecbugs-cli-${FSB_VERSION}.zip
RUN mkdir findsecbugs-cli
RUN unzip -d /findsecbugs-cli /tmp/findsecbugs-cli-${FSB_VERSION}.zip
RUN rm /tmp/findsecbugs-cli-${FSB_VERSION}.zip

COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
